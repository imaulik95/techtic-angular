import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DefaultLayoutComponent} from './containers/default-layout';
import {SimpleLayoutComponent} from './containers/simple-layout';
import {AuthGuard} from './guards';


export const routes: Routes = [
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'product',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/product/product.module').then(m => m.ProductModule)
      },
    ]
  },
  {
    path: 'auth',
    component: SimpleLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./views/authentication/authentication.module').then(m => m.AuthenticationModule)
      }
    ]
  },
  {
    path: '404',
    component: SimpleLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./views/error/404.module').then(m => m.P404Module)
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
