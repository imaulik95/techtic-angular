import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {DatatableService, HelperService, LoaderService} from '../../services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ProductService} from './product.service';

@Component({
  templateUrl: 'product.component.html',
})
export class ProductComponent implements OnInit, AfterViewInit, OnDestroy {
  users: any = [];
  modalRef: BsModalRef;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  localUser = HelperService.getLocalUser();
  public importForm: FormGroup;
  fileName;

  constructor(private datatableService: DatatableService,
              private _router: Router,
              private toaster: ToastrService,
              private modalService: BsModalService,
              private _postService: ProductService,
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.importForm = this._formBuilder.group({
      import_file: ['', Validators.required],
    });
    this.getAllProducts();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  getAllProducts() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 50,
      lengthMenu: [5, 10, 25, 50],
      serverSide: true,
      searchDelay: 1000,
      processing: true,
      language: {
        searchPlaceholder: 'Search...',
        search: ''
      },
      ajax: (dataTablesParameters: any, callback) => {
        this.datatableService.getTableData(dataTablesParameters, 'product/list').subscribe(resp => {
          this.users = resp.data;
          callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            data: []
          });
        });
      },
      order: [],
      columns: [
        {name: 'name'},
        {name: 'unique_code'},
        {name: 'description'},
        {name: 'category.name', orderable: false},
      ]
    };
  }

  onSelect(event) {
    this.fileName = event.target.files[0];
  }
  importModel(template) {
    this.modalRef = this.modalService.show(template);
  }

  closeModel() {
    this.importForm.reset();
    this.modalRef.hide();
  }

  importSubmit() {
    this.importForm.markAllAsTouched();
    if (this.importForm.valid) {
      const postValues = this.importForm.value;
      const propertyFormData = new FormData();
      propertyFormData.append('import_file', this.fileName);
      LoaderService.show();
      this._postService.importProduct(propertyFormData).subscribe(
        (response) => {
          LoaderService.hide();
          this.rerender();
          this.closeModel();
          if (Object.keys(response['data']).length === 0) {
            this.toaster.success('success', 'import successfully');
          } else {
            this.toaster.error('failed', 'compant not found');
          }
        },
        (e) => {
          LoaderService.hide();
          if (e['error']) {
            this.closeModel();
            this.setServeErrors(e['error']);
          }
        }
      );
    }
  }

  setServeErrors(errorResponse) {
    const arr: any = [];
    for (const controlKey of errorResponse.errors) {
      arr.push(controlKey);
    }
    this.toaster.error('error', arr);
  }

}
