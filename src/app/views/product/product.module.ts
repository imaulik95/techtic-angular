import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DataTablesModule} from 'angular-datatables';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormElementsModule} from '../../components/form-elements/form-elements.module';
import {SharedCustomModule} from '../../shared.module';
import {ProductComponent} from './product.component';
import {ProductRoutingModule} from './product-routing.module';
import {DropzoneModule} from '../../components/dropzone/dropzone.module';

@NgModule({
  declarations: [ProductComponent],
    imports: [
        CommonModule,
        ProductRoutingModule,
        DataTablesModule,
        SharedCustomModule,
        FormElementsModule,
        NgSelectModule,
        DropzoneModule
    ]
})
export class ProductModule {
}
