import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Injectable()
export class ProductService {

  constructor(private http: HttpClient) {
  }

  importProduct(postVal) {
    return this.http.post<any>(environment.api_url + 'product-import-file', postVal);
  }
}
