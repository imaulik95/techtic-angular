import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

import {AuthenticationService, StorageManagerService} from '../../../services';
import {userError} from '@angular/compiler-cli/src/transformers/util';


@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  public status = true;

  constructor(private route: Router,
              private authService: AuthenticationService,
              private _toasterService: ToastrService) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.registerForm = new FormGroup({
      first_name: new FormControl(null, [Validators.required]),
      last_name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required),
    });
  }

  onFormSubmit() {
    const credentials = this.registerForm.value;
    this.authService.register(credentials).subscribe(
      (responseData: any) => {
        // this.route.navigate(['/select-area']);
        if (responseData.success) {
          this._toasterService.success('success', 'Register Success');
        }
        this.route.navigate(['auth', 'login']);
      },
      (errorData) => {
        this._toasterService.error(errorData.error.message, 'Register Failed');
      });
  }


}
