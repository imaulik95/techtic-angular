import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {SharedCustomModule} from '../../shared.module';
import {AuthenticationRouting} from './authentication.routing';
import {RegisterComponent} from './register/register.component';


@NgModule({
  imports: [
    SharedCustomModule,
    AuthenticationRouting
  ],
  declarations: [LoginComponent, RegisterComponent]
})

export class AuthenticationModule {
}
