import {Component, OnInit} from '@angular/core';
import {navItems} from '../../_nav';
import {Router} from '@angular/router';
import {HelperService, StorageManagerService} from '../../services';
import _ from 'lodash';
@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  localUser = HelperService.getLocalUser();
  public minimized = false;
  public navItemsArray = [...navItems];
  navItems;

  constructor(private _router: Router) {

  }

  ngOnInit() {

      this.navItems = this.navItemsArray;
  }

  toggleMinimize(e) {
    this.minimized = e;
  }

  logout() {
    StorageManagerService.clearAll();
    this._router.navigate(['/', 'auth', 'login']);
  }
}
